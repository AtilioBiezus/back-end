package br.edu.unisep.store.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ProductDto {

    private final Integer id;

    private final String name;

    private final String description;

    private final Float price;

    private final String brand;

    private final Integer status;

    private final String statusDescription;

}
